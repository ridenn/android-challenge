package com.example.skuirrel.Data.utils

object Constants {

    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val API_KEY =  "cc545c21692815989a94a6de9fa4f90f"
    const val SESSION_ID =  "22c5b7ec28115b7fbd65ba66957b55f44845c755"
    const val ACC_ID = "61b0f7b25800c40019996a54"


    private const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/"

    const val YOUTUBE_THUMBNAIL_START_URL: String = "https://img.youtube.com/vi/"
    const val YOUTUBE_THUMBNAIL_END_URL: String = "/0.jpg"
    const val YOUTUBE_VIDEOS_BASE_URL = "https://www.youtube.com/watch?v="

    private const val IMAGE_SIZE_W342 = "w342"
    const val BASE_IMAGE_LARGE = BASE_IMAGE_URL + IMAGE_SIZE_W342
}